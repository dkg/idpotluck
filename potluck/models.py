from django.db import models
from django.core import validators


class Potluck(models.Model):
    NOT_STARTED = 1
    ID_COLLECTION = 2
    FILE_RETRIEVAL = 3
    HASH_CONFIRMATION = 4
    FILE_INSPECTION = 5
    INTRODUCTIONS = 6
    COMPLETED = 7
    POTLUCK_PHASES = (
        (NOT_STARTED, 'Not Started Yet'),
        (ID_COLLECTION, 'Collecting IDs'),
        (FILE_RETRIEVAL, 'Fetching file and calculating digest'),
        (HASH_CONFIRMATION, 'Confirming hashes'),
        (FILE_INSPECTION, 'Inspecting File'),
        (INTRODUCTIONS, 'In-person Introductions'),
        (COMPLETED, 'Completed'),
    )

    name = models.CharField(max_length=50)
    outputfile = models.CharField(max_length=20, validators=[validators.validate_slug])
    description = models.TextField(default='')
    pub_date = models.DateTimeField('date held', auto_now_add=True)
    phase = models.IntegerField(choices=POTLUCK_PHASES, default=NOT_STARTED)
    def advance(self):
        if (self.phase != COMPLETED):
            self.phase += 1
            


class CryptoIdentity(models.Model):
    potluck = models.ForeignKey(Potluck)
    name = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=100, blank=True, validators=[validators.validate_email])
    fingerprint = models.CharField(max_length=40) # FIXME: provide a way to clean up an OpenPGP fingerprint
    public_certs_ok = models.BooleanField()
    
