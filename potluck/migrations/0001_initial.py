# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CryptoIdentity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, blank=True)),
                ('email', models.CharField(blank=True, max_length=100, validators=[django.core.validators.EmailValidator()])),
                ('fingerprint', models.CharField(max_length=40)),
                ('public_certs_ok', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Potluck',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('outputfile', models.CharField(max_length=20, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('description', models.TextField(default=b'')),
                ('pub_date', models.DateTimeField(auto_now_add=True, verbose_name=b'date held')),
                ('phase', models.IntegerField(default=1, choices=[(1, b'Not Started Yet'), (2, b'Collecting IDs'), (3, b'Fetching file and calculating digest'), (4, b'Confirming hashes'), (5, b'Inspecting File'), (6, b'In-person Introductions'), (7, b'Completed')])),
            ],
        ),
        migrations.AddField(
            model_name='cryptoidentity',
            name='potluck',
            field=models.ForeignKey(to='potluck.Potluck'),
        ),
    ]
