from django.shortcuts import render, get_object_or_404
from models import Potluck

def index(request):
    current_potlucks = Potluck.objects.exclude(phase=Potluck.COMPLETED).order_by('-pub_date')[:5]
    context = {'current_potlucks': current_potlucks}
    return render(request, 'potluck/index.html', context)

def detail(request, potluck_id):
    potluck = get_object_or_404(Potluck, pk=potluck_id)
    return render(request, 'potluck/detail.html', {'potluck': potluck })
