from django.contrib import admin

from .models import Potluck, CryptoIdentity

admin.site.register(Potluck)
admin.site.register(CryptoIdentity)
